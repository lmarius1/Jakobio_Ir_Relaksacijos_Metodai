import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.MapValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Layouts extends LinearProgramming{
    private Stage window;
    public Layouts(Stage window){
        this.window = window;
    }

    public void loadScreen2() {
        window.setTitle("Jakobio ir Relaksacijos metodai");
        Button btnClear = new Button("Išvalyti");
        Button btnSkaiciuoti = new Button("Skaičiuoti");
        btnClear.setMinWidth(70);
        btnSkaiciuoti.setMinWidth(70);
        Button btnMinus = new Button("-");
        Button btnPlus = new Button("+");
        btnMinus.setMinWidth(30);
        btnPlus.setMinWidth(30);
        ComboBox<String> comboBoxPasirinkimas = new ComboBox<>();
        comboBoxPasirinkimas.getItems().addAll("Jakobio", "Relaksacijos");
        comboBoxPasirinkimas.setValue("Jakobio");
        comboBoxPasirinkimas.setId("1");    //cia pazymiu, kad jakobio. 2 bus relaksacijos
        VBox vPasir = new VBox(5);
        vPasir.setAlignment(Pos.BOTTOM_CENTER);
        vPasir.getChildren().addAll(new Label("Pasirinkite metodą"), comboBoxPasirinkimas);
        TextField tfE = new TextField() {
            @Override public void replaceText(int start, int end, String text) {
                // If the replaced text would end up being invalid, then simply
                // ignore this call!
                if (text.matches("[0-9.]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
                if (text.matches("[0-9.]*")) {
                    super.replaceSelection(text);
                }
            }

        };
        tfE.setPrefWidth(70);
        tfE.setMaxWidth(100);
        tfE.setOnKeyTyped(e -> {
            if(tfE.getId() == "color"){
                tfE.setId("color2");
            }
        });
        VBox vEPasir = new VBox(5);
        vEPasir.setAlignment(Pos.BOTTOM_CENTER);
        vEPasir.getChildren().addAll(new Label("e"), tfE);
        vEPasir.setMinWidth(70);


        HBox hBox = new HBox(10);
        hBox.setAlignment(Pos.BOTTOM_CENTER);
        hBox.getChildren().addAll(btnMinus, btnPlus, btnClear, btnSkaiciuoti, vEPasir, vPasir);

        TextField tfRelReiksm = new TextField() {
            @Override public void replaceText(int start, int end, String text) {
                // If the replaced text would end up being invalid, then simply
                // ignore this call!
                if (text.matches("[0-9.]*")) {
                    super.replaceText(start, end, text);
                }
            }

            @Override public void replaceSelection(String text) {
                if (text.matches("[0-9.]*")) {
                    super.replaceSelection(text);
                }
            }
        };
        tfRelReiksm.setPrefWidth(70);
        tfRelReiksm.setMaxWidth(100);
        /*tfRelReiksm.setOnKeyTyped(e -> {
            if(tfRelReiksm.getId().equals("color")){
                tfRelReiksm.setId("color2");
            }
        });*/
        tfRelReiksm.setOnKeyTyped(e -> {
            try {
                if (tfRelReiksm.getId().equals("color"))
                    tfRelReiksm.setId("color2");
            } catch (Exception exc){};
        });

        VBox vbRelReiksm = new VBox(5);
        vbRelReiksm.setAlignment(Pos.BOTTOM_CENTER);
        Label lbRelReiksm = new Label("\u0428");
        vbRelReiksm.getChildren().addAll(lbRelReiksm, tfRelReiksm);

        comboBoxPasirinkimas.setOnAction(e -> {
            if(comboBoxPasirinkimas.getValue().equals("Relaksacijos")){
                if(comboBoxPasirinkimas.getId().equals("1")){
                    hBox.getChildren().add(vbRelReiksm);
                    comboBoxPasirinkimas.setId("2");
                }
            } else {
                if(comboBoxPasirinkimas.getId().equals("2")){
                    hBox.getChildren().remove(vbRelReiksm);
                    comboBoxPasirinkimas.setId("1");
                    tfRelReiksm.setText("");
                }
            }
        });

        BorderPane borderPane = new BorderPane();
        hBox.setPadding(new Insets(20, 0, 0, 0));
        borderPane.setTop(hBox);

        ArrayList<ArrayList<TextField>> fields = createTextField(3);
        ArrayList<TextField> vector = createVector(3);
        HBox fieldsSP = createLinearTable(fields, vector);

        VBox centerVBox = new VBox(30); //sis VBox laikys tiesiniu lygciu sistemos langelius ir taip pat atsakymus.
        centerVBox.getChildren().add(fieldsSP);
        borderPane.setCenter(centerVBox);

        HBox answerHBox = new HBox(); //Kai pridedamas answer, bus dedama prie center VBox.
        answerHBox.setAlignment(Pos.TOP_CENTER);

        btnPlus.setOnAction(e -> {
            addLineToTF(fields, vector);
            centerVBox.getChildren().clear();
            centerVBox.getChildren().add(createLinearTable(fields, vector));
            System.gc();    //callinamas garbage collector, kad ismestu nereikalinga memory.
        });
        btnMinus.setOnAction(e -> {
            removeLineFromTF(fields, vector);
            centerVBox.getChildren().clear();
            centerVBox.getChildren().add(createLinearTable(fields, vector));
            System.gc();
        });
        btnClear.setOnAction(e -> {
            clearFields(fields, vector, tfE, tfRelReiksm);
        });
        btnSkaiciuoti.setOnAction(e -> {
            boolean arBuvoRaudona = false;
            boolean arRel = comboBoxPasirinkimas.getId().equals("2");
            BigDecimal skE = null;
            BigDecimal skS = null;
            String s = tfE.getText();
            int tipas = super.checkIfNumber(s);
            if(tipas == -1){
                tfE.setId("color");
                arBuvoRaudona = true;
                new AlertBox("Neteisingai įvesta e reikšmė.", window, true);
            } else {
                skE = new BigDecimal(s);
            }
            if(!arBuvoRaudona){
                if(arRel){
                    String s2 = tfRelReiksm.getText();
                    int tipas2 = super.checkIfNumber(s2);
                    boolean arRastiParametra = false;
                    if(tipas2 == -1){   //Jei w reiksme neivesta, tuomet randam optimalia.
                        arRastiParametra = true;
                    } else {
                        skS = new BigDecimal(s2);
                        if (skS.compareTo(new BigDecimal(0)) <= 0 || skS.compareTo(new BigDecimal(2)) >= 0) {
                            tfRelReiksm.setId("color");
                            arBuvoRaudona = true;
                            new AlertBox("Neteisingai įvesta ш reikšmė.\nш turi priklausyti intervalui (0,2)\nJei norite, kad butu naudojama optimali ш reikšmė, palikite šį langelį tuščią.", window, true);
                        }
                    }
                    if(!arBuvoRaudona) {
                        BigRational[][] arr = getArray(fields, vector);
                        if (arr == null) {
                            new AlertBox("Neteisingai įvesti tiesinės lygties duomenys.", window);
                        } else {
                            if(arRastiParametra){   //jei nebuvo naudotas joks parametras, tai bus rastas.
                                skS = super.getRelaxationParameter(arr);
                                if(skS.compareTo(new BigDecimal("-1")) != 0){
                                    tfRelReiksm.setText(skS.toString());
                                }
                            }
                            ArrayList<BigDecimal[]> ats = super.solveRelaxation(arr, skE, skS);
                            if (ats == null) {
                                new AlertBox("Sistema diverguoja, todėl metodas negali būti įvykdytas.", window, true);
                            } else {
                                TableView table = createTableView(ats);
                                if (!answerHBox.getChildren().isEmpty()) {
                                    try {
                                        centerVBox.getChildren().remove(answerHBox); //panaikinamas sis HBox
                                        answerHBox.getChildren().clear();
                                    } catch (Exception exc) {
                                    }
                                }
                                answerHBox.getChildren().add(table);
                                answerHBox.setAlignment(Pos.TOP_CENTER);
                                centerVBox.getChildren().addAll(answerHBox);
                                System.gc();
                            }
                        }
                    }
                }
                else {
                    BigRational[][] arr = getArray(fields, vector);
                    if(arr == null) {
                        new AlertBox("Neteisingai įvesti tiesinės lygties duomenys.", window);
                    } else {
                        ArrayList<BigDecimal[]> ats = super.solveJacobi(arr, skE);
                        if(ats == null){
                            new AlertBox("Sistema diverguoja, todėl metodas negali būti įvykdytas.", window, true);
                        } else {
                            TableView table = createTableView(ats);
                            try {
                                ScrollBar scrollBarv = (ScrollBar) table.lookup(".scroll-bar:vertical");
                                scrollBarv.setDisable(true);
                            } catch (Exception exc){};
                            if(!answerHBox.getChildren().isEmpty()){
                                try {
                                    centerVBox.getChildren().remove(answerHBox); //panaikinamas sis HBox
                                    answerHBox.getChildren().clear();
                                } catch (Exception exc){}
                            }
                            answerHBox.getChildren().add(table);
                            answerHBox.setAlignment(Pos.TOP_CENTER);
                            centerVBox.getChildren().addAll(answerHBox);
                            System.gc();
                        }
                    }
                }
            }
        });
        Scene scene = new Scene(borderPane);
        scene.getStylesheets().addAll("JavaFXCSSConfig.css");
        window.setScene(scene);
        window.show();
    }



    private LinkedList<String> createColumnHeaders(ArrayList<BigDecimal[]> ats){
        int n = ats.size()-1;
        int k = ats.get(n)[0].intValueExact();
        int pradzia = k-n;
        int pabaiga = pradzia+n;
        LinkedList<String> columns = new LinkedList<>();
        columns.add("k");
        for(int i = pradzia; i < pabaiga; i++){
            columns.add(Integer.toString(i+1));
        }
        return columns;
    }

    private TableView createTableView(ArrayList<BigDecimal[]> ats){
        LinkedList<String> cols = createColumnHeaders(ats);
        TableView table_view = new TableView<>(generateDataInMap(ats, cols));

        int n = cols.size();
        for(int i = 0; i < n-1; i++){
            TableColumn<Map, String> firstDataColumn = new TableColumn<>(cols.get(i));
            firstDataColumn.setCellValueFactory(new MapValueFactory(cols.get(i)));
            firstDataColumn.setMinWidth(50);
            table_view.getColumns().add(firstDataColumn);
        }
        TableColumn<Map, String> firstDataColumn = new TableColumn<>(cols.get(n-1));
        firstDataColumn.setCellValueFactory(new MapValueFactory(cols.get(n-1)));
        firstDataColumn.setMinWidth(50);
        table_view.getColumns().add(firstDataColumn);

        table_view.setFixedCellSize(25);
        table_view.prefHeightProperty().bind(Bindings.size(table_view.getItems()).multiply(table_view.getFixedCellSize()).add(40));
        return table_view;
    }

    private ObservableList<Map> generateDataInMap(ArrayList<BigDecimal[]> answ, LinkedList<String> cols) {
        int k = cols.size()-1;
        int eiluciu = answ.get(0).length;
        ObservableList<Map> allData = FXCollections.observableArrayList();
        Map<String, String> dataRow;

        for(int i = 0; i < eiluciu; i++){
            dataRow = new HashMap<>();
            dataRow.put("k", "x"+Integer.toString(i+1));
            for(int j = 0; j < k; j++){
                dataRow.put(cols.get(j+1), answ.get(j)[i].toString());
            }
            allData.add(dataRow);
        }
        return allData;
    }

    private void clearFields(ArrayList<ArrayList<TextField>> fields, ArrayList<TextField> vector, TextField E, TextField rel) {
        int n = fields.size();
        BigRational[][] arr = new BigRational[n][n+1];
        boolean arBuvoRaudona = false;
        for(int i = 0; i < n; i++){
            TextField tf;
            for(int j = 0; j < n; j++) {
                fields.get(i).get(j).clear();
            }
            vector.get(i).clear();
        }
        E.clear();
        rel.clear();
    }

    private BigRational[][] getArray(ArrayList<ArrayList<TextField>> fields, ArrayList<TextField> vector) {
        int n = fields.size();
        BigRational[][] arr = new BigRational[n][n+1];
        boolean arBuvoRaudona = false;
        for(int i = 0; i < n; i++){
            TextField tf;
            for(int j = 0; j < n; j++){
                tf = fields.get(i).get(j);
                String s = tf.getText();
                int tipas = super.checkIfNumber(s);
                if(tipas == -1){
                    tf.setId("color");
                    arBuvoRaudona = true;
                } else if(arBuvoRaudona){}  //tiesiog catchinam ir nieko nesaugom, jei buvo raudona
                else if(tipas == 1){
                    arr[i][j] = new BigRational(BigInteger.valueOf(Integer.parseInt(s)), BigInteger.valueOf(1));
                } else if(tipas == 2){
                    arr[i][j] = super.convertBigDecimalToBigRational(new BigDecimal(s));
                } else {
                    arr[i][j] = super.getBigRatFromFracString(s);
                }
            }
            tf = vector.get(i);
            String s = tf.getText();
            int tipas = super.checkIfNumber(s);
            if(tipas == -1){
                tf.setId("color");
                arBuvoRaudona = true;
            } else if(arBuvoRaudona){}  //tiesiog catchinam ir nieko nesaugom, jei buvo raudona
            else if(tipas == 1){
                arr[i][n] = new BigRational(BigInteger.valueOf(Integer.parseInt(s)), BigInteger.valueOf(1));
            } else if(tipas == 2){
                arr[i][n] = super.convertBigDecimalToBigRational(new BigDecimal(s));
            } else {
                arr[i][n] = super.getBigRatFromFracString(s);
            }
        }
        if(arBuvoRaudona)
            return null;
        return arr;
    }

    public ArrayList<TextField> createVector(int kiek){
        ArrayList<TextField> vector = new ArrayList<>();
        for(int i = 0; i < kiek; i++){
            TextField tf = new TextField() {
                @Override public void replaceText(int start, int end, String text) {
                    if (text.matches("[0-9./-]*")) {
                        super.replaceText(start, end, text);
                    }
                }
                @Override public void replaceSelection(String text) {
                    if (text.matches("[0-9./-]*")) {
                        super.replaceSelection(text);
                    }
                }
            };

            tf.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    double diff = tf.getWidth() - tf.getText().length() * 8.7;
                    if(diff < 0)
                        tf.setPrefWidth(tf.getText().length() * 8.7);
                }
            });
            tf.setPrefWidth(50);
            tf.setMaxWidth(100);
            tf.setOnKeyTyped(e -> {
                if(tf.getId() == "color"){
                    tf.setId("color2");
                }
            });

            vector.add(tf);
        }
        return vector;
    }

    public ArrayList<ArrayList<TextField>> createTextField(int kiek){
        ArrayList<ArrayList<TextField>> fields = new ArrayList<>();
        for(int i = 0; i < kiek; i++){
            ArrayList<TextField> arr = new ArrayList<>();
            for(int j = 0; j < kiek; j++){
                TextField tf = new TextField() {
                    @Override public void replaceText(int start, int end, String text) {
                        if (text.matches("[0-9./-]*")) {
                            super.replaceText(start, end, text);
                        }
                    }
                    @Override public void replaceSelection(String text) {
                        if (text.matches("[0-9./-]*")) {
                            super.replaceSelection(text);
                        }
                    }
                };

                tf.textProperty().addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                        double diff = tf.getWidth() - tf.getText().length() * 8.7;
                        if(diff < 0)
                            tf.setPrefWidth(tf.getText().length() * 8.7);
                    }
                });
                tf.setPrefWidth(50);
                tf.setMaxWidth(100);
                tf.setOnKeyTyped(e -> {
                    if(tf.getId() == "color"){
                        tf.setId("color2");
                    }
                });

                arr.add(tf);
            }
            fields.add(arr);
        }
        return fields;
    }

    public void removeLineFromTF(ArrayList<ArrayList<TextField>> fields, ArrayList<TextField> sk){
        int n = fields.size();
        if(n == 1){
            return;
        }
        for(int i = 0; i < n-1; i++){   //istrinami visur paskutiniai eiluciu elementai
            fields.get(i).remove(n-1);
        }
        fields.remove(n-1);
        sk.remove(n-1);
    }

    public void addLineToTF(ArrayList<ArrayList<TextField>> fields, ArrayList<TextField> sk){
        int n = fields.size();
        if(n > 20){
            return;
        }
        for(ArrayList<TextField> tfs : fields){
            TextField tf = new TextField() {
                @Override public void replaceText(int start, int end, String text) {
                    if (text.matches("[0-9./-]*")) {
                        super.replaceText(start, end, text);
                    }
                }
                @Override public void replaceSelection(String text) {
                    if (text.matches("[0-9./-]*")) {
                        super.replaceSelection(text);
                    }
                }
            };

            tf.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    double diff = tf.getWidth() - tf.getText().length() * 8.7;
                    if(diff < 0)
                        tf.setPrefWidth(tf.getText().length() * 8.7);
                }
            });
            tf.setPrefWidth(50);
            tf.setMaxWidth(100);
            tf.setOnKeyTyped(e -> {
                if(tf.getId() == "color"){
                    tf.setId("color2");
                }
            });

            tfs.add(tf);
        }
        ArrayList<TextField> arr = new ArrayList<>();
        for(int i = 0; i < n+1; i++){
            TextField tf = new TextField() {
                @Override public void replaceText(int start, int end, String text) {
                    if (text.matches("[0-9./-]*")) {
                        super.replaceText(start, end, text);
                    }
                }
                @Override public void replaceSelection(String text) {
                    if (text.matches("[0-9./-]*")) {
                        super.replaceSelection(text);
                    }
                }
            };

            tf.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    double diff = tf.getWidth() - tf.getText().length() * 8.7;
                    if(diff < 0)
                        tf.setPrefWidth(tf.getText().length() * 8.7);
                }
            });
            tf.setPrefWidth(50);
            tf.setMaxWidth(100);
            tf.setOnKeyTyped(e -> {
                if(tf.getId() == "color"){
                    tf.setId("color2");
                }
            });

            arr.add(tf);
        }
        fields.add(arr);

        TextField tf = new TextField() {
            @Override public void replaceText(int start, int end, String text) {
                if (text.matches("[0-9./-]*")) {
                    super.replaceText(start, end, text);
                }
            }
            @Override public void replaceSelection(String text) {
                if (text.matches("[0-9./-]*")) {
                    super.replaceSelection(text);
                }
            }
        };

        tf.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                double diff = tf.getWidth() - tf.getText().length() * 8.7;
                if(diff < 0)
                    tf.setPrefWidth(tf.getText().length() * 8.7);
            }
        });
        tf.setPrefWidth(50);
        tf.setMaxWidth(100);
        tf.setOnKeyTyped(e -> {
            if(tf.getId() == "color"){
                tf.setId("color2");
            }
        });

        sk.add(tf);
    }

    public HBox createLinearTable(ArrayList<ArrayList<TextField>> fields, ArrayList<TextField> vector){
        int n = fields.size();
        VBox finalTable = new VBox(10);
        for(int i = 0; i < n; i++){
            HBox hb = new HBox(1);
            hb.setAlignment(Pos.CENTER);
            for(int j = 0; j < n-1; j++){
                Label xval = new Label("x"+Integer.toString(j+1));
                Label plusSign = new Label("+");
                plusSign.setPadding(new Insets(0, 5, 0, 5));
                hb.getChildren().addAll(fields.get(i).get(j), xval, plusSign);
            }
            Label xv = new Label("x"+Integer.toString(n));
            xv.setPadding(new Insets(0, 9, 0, 0));
            hb.getChildren().addAll(fields.get(i).get(n-1), xv);
            Label equalSign = new Label("=");
            equalSign.setPadding(new Insets(0, 9, 0, 0));
            hb.getChildren().addAll(equalSign, vector.get(i));
            finalTable.getChildren().addAll(hb);
        }
        ScrollPane sp = new ScrollPane(finalTable);
        sp.setStyle("-fx-background-color:transparent;");
        HBox hbR = new HBox(sp);
        hbR.setAlignment(Pos.CENTER);
        hbR.setPadding(new Insets(15, 0, 0, 0));
        return hbR;
    }
}
