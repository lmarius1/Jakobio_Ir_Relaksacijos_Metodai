import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;

import static java.awt.SystemColor.text;

/**
 * Created by Marius Lk on 2016-09-12.
 */
public class LinearProgramming {

    protected BigRational convertBigDecimalToBigRational(BigDecimal number){
        String s = number.toString();
        int digitsDec = s.length() - 1 - s.indexOf('.');
        int denom = 1;
        for (int i = 0; i < digitsDec; i++) {
            number = number.multiply(new BigDecimal("10"));
            denom *= 10;
        }

        int num = number.intValue();
        BigRational bigRational = new BigRational(BigInteger.valueOf(num), BigInteger.valueOf(denom));

        return bigRational;
    }

    protected int checkIfNumber(String num){  //patikrinama ar given string galima konvertuoti i skaiciu. Jei taip, koki (integer, double, fractional)
        if(num.length() == 0)
            return -1;
        if(num.charAt(0) != 45 && (num.charAt(0) < 48 || num.charAt(0) > 57))   //jei pirmas skaicius nera nei '-', nei skaicius, tuomet tai nera skaicius.
            return -1;

        boolean arJauSeperated = false; //reikalingas tam, kad patikrintu '.' ar '/' simbolius. Kadangi sk gali buti 1.22 arba 1/2 pvz.
        int kurisAtskirtas = 0; //pasakys ar buvo pagautas '.', ar '/' simbolis.

        for(int i = 1; i < num.length() -1; i++){
            char tmp = num.charAt(i);
            if(tmp < 48 || tmp > 57){
                if(arJauSeperated)
                    return -1;
                if(tmp == 46) {     //pasizymiu, kad atskirtas '.' simbolis.
                    arJauSeperated = true;
                    kurisAtskirtas = 1;
                } else if(tmp == 47) {
                    arJauSeperated = true;
                    kurisAtskirtas = 2;
                } else
                    return -1;      //cia reiskia, jog
            }
        }
        char lastChar = num.charAt(num.length()-1);
        if(lastChar < 48 || lastChar > 57)
            return -1;

        if(arJauSeperated == false) //returning integer
            return 1;

        if(kurisAtskirtas == 1)     //returning double
            return 2;

        for(int i = 1; i < num.length()-1; i++){    //dalyba is 0 negalima
            if(num.charAt(i) == 47){
                if(num.charAt(i+1) == '0'){
                    return -1;
                }
                return 3;
            }
        }

        return 3;               //returning fraction
    }

    //siaip cia visuomet turetu splitintas but ilgio 2, kadangi yra paleidziamas tik pracheckintas su 'check if number' funkcija, kuri returnina 3 (kas reiskia exactly 1 '/').
    protected BigRational getBigRatFromFracString(String frac){
        String[] tokens = frac.split("/");
        if(tokens.length == 2){
            BigRational bigRational = new BigRational(BigInteger.valueOf(Integer.parseInt(tokens[0])), BigInteger.valueOf(Integer.parseInt(tokens[1])));
            return  bigRational;
        }
        return null;
    }

    protected ArrayList<BigDecimal[]> solveRelaxation(BigRational[][] arr, BigDecimal E, BigDecimal w){
        if(arKonverguoja(arr)){
            ArrayList<BigDecimal[]> ats = solveRelaxation1(arr, E, 10000, w);
            if(ats != null) {
                int skPoKablelio = skPoKablelio(E);
                roundNumbers(ats, skPoKablelio+1);    //atsakymas roundinamas pagal tai, kiek yra sk po kablelio
            }
            return ats;
        }
        return null;
    }

    private ArrayList<BigDecimal[]> solveRelaxation1(BigRational[][] arr, BigDecimal E, int kiekSaugom, BigDecimal w){
        int n = arr.length;
        BigDecimal[] ats = new BigDecimal[n];
        for(int i = 0; i < n; i++){
            ats[i] = new BigDecimal("0");
        }
        BigDecimal[][] mas = new BigDecimal[n][n+1];
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n+1; j++){
                mas[i][j] = arr[i][j].toBigDecimal(1000, RoundingMode.HALF_EVEN);
            }
        }

        MathContext mc = new MathContext(1000, RoundingMode.HALF_EVEN); //apvalins tiek toliau BigDecimal values
        int k = 1;
        BigDecimal one = new BigDecimal("-1");

        ArrayList<BigDecimal[]> answers = new ArrayList<>();

        for(int q = 0; q < kiekSaugom; q++){
            System.out.println("k: " + k++);
            BigDecimal[] tmpAts = new BigDecimal[n];
            for(int i = 0; i < n; i++){
                BigDecimal tmpArt = new BigDecimal("0");

                for(int j = 0; j < i; j++){
                    tmpArt = tmpArt.add(mas[i][j].multiply(tmpAts[j], mc)); //cia bus k+1 artinys
                }
                for(int j = i+1; j < n; j++){
                    tmpArt = tmpArt.add(mas[i][j].multiply(ats[j], mc));    //cia bus k artinys
                }
                tmpArt = tmpArt.subtract(mas[i][n]);
                tmpArt = tmpArt.multiply(one.divide(mas[i][i], mc));   //is -1/mas[i][i] dauginu cia
                tmpAts[i] = ats[i].add(w.multiply(tmpArt.subtract(ats[i])));
                System.out.println(tmpAts[i]);
            }
            if(getDiff(ats, tmpAts).compareTo(E) <= 0){
                System.out.println("TIESA!!! k: " + (k-1));
                System.out.println("E: " + E);
                for(int z = 0; z < n; z++){
                    System.out.println(tmpAts[z]);
                }
                BigDecimal[] size = new BigDecimal[1];
                size[0] = new BigDecimal(k-1);
                answers.add(tmpAts);
                answers.add(size);  //paskutinis atsakymu liste laiko to size dydi, kad zinociau kokios k reiksmes.
                return answers;
            }

            answers.add(tmpAts);
            ats = tmpAts;
            System.out.println("__________________________________________");
        }

        while(true){
            System.out.println("k: " + k++);
            BigDecimal[] tmpAts = new BigDecimal[n];
            for(int i = 0; i < n; i++){
                BigDecimal tmpArt = new BigDecimal("0");

                for(int j = 0; j < i; j++){
                    tmpArt = tmpArt.add(mas[i][j].multiply(tmpAts[j], mc)); //cia bus k+1 artinys
                }
                for(int j = i+1; j < n; j++){
                    tmpArt = tmpArt.add(mas[i][j].multiply(ats[j], mc));    //cia bus k artinys
                }
                tmpArt = tmpArt.subtract(mas[i][n]);
                tmpArt = tmpArt.multiply(one.divide(mas[i][i], mc));   //is -1/mas[i][i] dauginu cia
                tmpAts[i] = ats[i].add(w.multiply(tmpArt.subtract(ats[i])));
                System.out.println(tmpAts[i]);
            }
            if(getDiff(ats, tmpAts).compareTo(E) <= 0){
                System.out.println("TIESA!!! k: " + (k-1));
                System.out.println("E: " + E);
                for(int z = 0; z < n; z++){
                    System.out.println(tmpAts[z]);
                }
                BigDecimal[] size = new BigDecimal[1];
                size[0] = new BigDecimal(k-1);
                answers.remove(0);
                answers.add(tmpAts);
                answers.add(size);  //paskutinis atsakymu liste laiko to size dydi, kad zinociau kokios k reiksmes.
                return answers;
            }

            answers.remove(0);
            answers.add(tmpAts);
            ats = tmpAts;
            System.out.println("__________________________________________");
        }
    }

    private int skPoKablelio(BigDecimal E){
        try{    //meginu daryti convert i integer (bus exception, jei bus bent vienas sk po kablelio ne 0
            int a = E.intValueExact();
            return 0;
        } catch (Exception exc){
            String text = E.abs().toString();

            int places = text.indexOf('-'); //jei yra cia -, tuomet reiskia, kad bus ...E-XXXX
            if(places != -1){
                return Integer.parseInt(text.substring(places+1));
            }
            int integerPlaces = text.indexOf('.');
            int decimalPlaces = text.length() - integerPlaces - 1;
            return decimalPlaces;
        }
    }

    private void roundNumbers(ArrayList<BigDecimal[]> ats, int sk){
        int k = ats.size();
        int n = ats.get(0).length;
        for(int i = 0; i < k-1; i++){   //paskutines k reiksmes ir neimu...
            for(int j = 0; j < n; j++){
                if(arDidinsim(ats.get(i)[j], sk)){
                    ats.get(i)[j] = ats.get(i)[j].setScale(sk, RoundingMode.HALF_UP);
                } else {
                    ats.get(i)[j] = ats.get(i)[j].setScale(sk, RoundingMode.HALF_DOWN);
                }
            }
        }
    }

    private boolean arDidinsim(BigDecimal bd, int sk){
        //cia jokiais budais negalima papulti, kai skaicius neturi '.' char, nes tuomet bus klaidu.
        //jei neturi tasko, tai nereikia ir apvalinti, nereikia ir kviesti sios funkcijos.
        String s = bd.toString();
        int taskas = s.indexOf('.');
        if(taskas != -1) {
            if (s.length() - 1 > taskas + sk) {
                if (s.charAt(taskas + sk) % 2 == 0) {
                    return false;   //lyginis, tad mazinsim, kai 5
                } else {
                    return true;    //nelyginis skaicius, tad didinsim, kai 5
                }
            }
        }
        return false;   //doesn't matter, kadangi vis vien nesiapvalins skaiciai
    }

    protected ArrayList<BigDecimal[]> solveJacobi(BigRational[][] arr, BigDecimal E){
        if(arKonverguoja(arr)){
            int skPoKablelio = skPoKablelio(E);
            ArrayList<BigDecimal[]> ats = solveJacobi1(arr, E, 1000, skPoKablelio);
            if(ats != null) {
                roundNumbers(ats, skPoKablelio+1);
            }
            return ats;
        }
        return null;
    }
    private ArrayList<BigDecimal[]> solveJacobi1(BigRational[][] arr, BigDecimal E, int kiekSaugom, int skPoKablelio){
        int n = arr.length;
        BigDecimal[] ats = new BigDecimal[n];
        for(int i = 0; i < n; i++){
            ats[i] = new BigDecimal("0");
        }
        BigDecimal[][] mas = new BigDecimal[n][n+1];
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n+1; j++){
                mas[i][j] = arr[i][j].toBigDecimal(1000, RoundingMode.HALF_EVEN);
            }
        }
        int q = 0;
        MathContext mc = new MathContext(1000, RoundingMode.HALF_EVEN); //apvalins tiek toliau BigDecimal values
        for(int i = 0; i < n; i++){     //issireiskiu istrizaines reiksmes, dalindamas kitus eilutes narius is jos elementu.
            for(int j = 0; j < i; j++){
                mas[i][j] = mas[i][j].divide(mas[i][i], mc);
                System.out.print(arr[i][j] + "   ");
            }
            for(int j = i+1; j < n; j++){
                mas[i][j] = mas[i][j].divide(mas[i][i], mc);
            }
            mas[i][n] = mas[i][n].divide(mas[i][i], mc);
            System.out.println(arr[i][n]);
        }
        int k = 1;
        ArrayList<BigDecimal[]> answers = new ArrayList<>();
        for(int w = 0; w < kiekSaugom; w++){
            System.out.println("k: " + k++);
            BigDecimal[] tmpAts = new BigDecimal[n];
            for(int i = 0; i < n; i++){
                BigDecimal tmpA = new BigDecimal("0");
                for(int j = 0; j < i; j++){
                    tmpA = tmpA.subtract(mas[i][j].multiply(ats[j]));
                }
                for(int j = i+1; j < n; j++){
                    tmpA = tmpA.subtract(mas[i][j].multiply(ats[j]));
                }
                tmpA = tmpA.add(mas[i][n]);
                tmpAts[i] = tmpA;
                System.out.println(tmpA);
            }
            BigDecimal diff = getDiff(ats, tmpAts);
            System.out.println("Diff: " + diff);
            if(diff.compareTo(E) <= 0) {
                BigDecimal[] size = new BigDecimal[1];
                size[0] = new BigDecimal(k-1);
                answers.add(tmpAts);
                answers.add(size);
                System.out.println("TIESA!!! k: " + (k-1));
                System.out.println("E: " + E);
                for(int z = 0; z < n; z++){
                    System.out.println(tmpAts[z]);
                }
                return answers;
            }

            answers.add(tmpAts);
            ats = tmpAts;
            System.out.println("------------------------------");
        }
        while(true){
            System.out.println("k: " + k++);
            BigDecimal[] tmpAts = new BigDecimal[n];
            for(int i = 0; i < n; i++){
                BigDecimal tmpA = new BigDecimal("0");
                for(int j = 0; j < i; j++){
                    tmpA = tmpA.subtract(mas[i][j].multiply(ats[j]));
                }
                for(int j = i+1; j < n; j++){
                    tmpA = tmpA.subtract(mas[i][j].multiply(ats[j]));
                }
                tmpA = tmpA.add(mas[i][n]);
                tmpAts[i] = tmpA;
                System.out.println(tmpA);
            }
            BigDecimal diff = getDiff(ats, tmpAts);
            System.out.println("Diff: " + diff);
            if(diff.compareTo(E) <= 0) {
                System.out.println("TIESA!!! k: " + (k-1));
                System.out.println("E: " + E);
                for(int z = 0; z < n; z++){
                    System.out.println(tmpAts[z]);
                }
                BigDecimal[] size = new BigDecimal[1];
                size[0] = new BigDecimal(k-1);
                answers.remove(0);
                answers.add(tmpAts);
                answers.add(size);  //paskutinis atsakymu liste laiko to size dydi, kad zinociau kokios k reiksmes.
                return answers;
            }

            answers.remove(0);  //istrinamas seniausiais ir idedamas naujos iteracijos atsakymas
            answers.add(tmpAts);
            ats = tmpAts;
            System.out.println("------------------------------");
        }
    }

    private BigDecimal[] artiniuDiffJacobi(BigRational[][] arr){
        int n = arr.length;
        BigDecimal[] ats = new BigDecimal[n];
        for(int i = 0; i < n; i++){
            ats[i] = new BigDecimal("0");
        }
        BigDecimal[][] mas = new BigDecimal[n][n+1];
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n+1; j++){
                mas[i][j] = arr[i][j].toBigDecimal(1000, RoundingMode.HALF_EVEN);
            }
        }
        int q = 0;
        MathContext mc = new MathContext(1000, RoundingMode.HALF_EVEN); //apvalins tiek toliau BigDecimal values
        for(int i = 0; i < n; i++){     //issireiskiu istrizaines reiksmes, dalindamas kitus eilutes narius is jos elementu.
            for(int j = 0; j < i; j++){
                mas[i][j] = mas[i][j].divide(mas[i][i], mc);
            }
            for(int j = i+1; j < n; j++){
                mas[i][j] = mas[i][j].divide(mas[i][i], mc);
            }
            mas[i][n] = mas[i][n].divide(mas[i][i], mc);
        }
        BigDecimal[] answers = new BigDecimal[2];   //0 saugos |X^(0) - X^(1)|, o 1 tarp 2 ir pirmo artinio.
        for(int w = 0; w < 2; w++){
            BigDecimal[] tmpAts = new BigDecimal[n];
            for(int i = 0; i < n; i++){
                BigDecimal tmpA = new BigDecimal("0");
                for(int j = 0; j < i; j++){
                    tmpA = tmpA.subtract(mas[i][j].multiply(ats[j]));
                }
                for(int j = i+1; j < n; j++){
                    tmpA = tmpA.subtract(mas[i][j].multiply(ats[j]));
                }
                tmpA = tmpA.add(mas[i][n]);
                tmpAts[i] = tmpA;
            }
            answers[w] = getDiff(ats, tmpAts);
            ats = tmpAts;
            System.out.println(answers[w]);
        }
        //cia turetu buti tas while(true) ir tuomet surasti ta "kai tik du gretimi artiniai tampa pakankamai artimi)....
        /*while(true){

        }*/
        return answers;
    }

    protected BigDecimal getRelaxationParameter(BigRational[][] arr){
        if(!arKonverguoja(arr)){
            return new BigDecimal("-1");
        }
        BigDecimal[] artiniaiDiff = artiniuDiffJacobi(arr); //[0] bus max|1-0| artinys, [1] max|2-1|.
        if(artiniaiDiff[0].compareTo(new BigDecimal(0)) == 0){
            return new BigDecimal("1"); //dalyba is nulio negalima, o nulinis artinis = pirmam artiniui. Tad returninu w/e ir paskiau iskart bus surasta tiksli reiksme.
        }
        MathContext mc = new MathContext(1000, RoundingMode.HALF_EVEN);
        BigDecimal p = artiniaiDiff[1].divide(artiniaiDiff[0], mc);
        p = p.multiply(p);  //pakeliamas kvadratu
        if(p.compareTo(new BigDecimal("1")) > 0){
            return new BigDecimal("1");
            //jei >1, tuomet traukiant sakni su (-) zenklu gausime error. Tad tiesiog returninu su 1 parametra,
            // o tuomet vis vien turetu nepraeiti konvergavimo taisykliu tikrinimo.
        }
        p = new BigDecimal("1").subtract(p);
        double sqrt = Math.sqrt((p.doubleValue()));
        p = (new BigDecimal("1").divide(new BigDecimal("1").add(new BigDecimal(sqrt)), mc)).setScale(5, RoundingMode.HALF_EVEN);
        return p;
    }

    private BigDecimal getDiff(BigDecimal[] prevAts, BigDecimal[] curAts){
        BigDecimal maxDiff = prevAts[0].subtract(curAts[0]).abs();
        int n = prevAts.length;
        for(int i = 1; i < n; i++){
            BigDecimal diff = prevAts[i].subtract(curAts[i]).abs();
            if(diff.compareTo(maxDiff) > 0){
                maxDiff = diff;
            }
        }
        return maxDiff;
    }

    private boolean arKonverguoja(BigRational[][] arr){
        int n = arr.length;
        int[] table = new int[n];

        boolean arKeisim = false;
        for(int i = 0; i < n; i++){
            int maxInd = 0;
            BigRational sum = new BigRational(BigInteger.valueOf(0), BigInteger.valueOf(1));
            BigRational max = arr[i][0].abs();
            for(int j = 1; j < n; j++){
                BigRational sk = arr[i][j].abs();
                if(sk.compareTo(max) > 0){
                    sum = sum.add(max);
                    maxInd = j;
                    max = sk;
                } else {
                    sum = sum.add(sk);
                }
            }
            if(table[maxInd] == 0 && max.compareTo(sum) > 0) {
                table[maxInd] = i + 1;   //surasau indeksus arr[][], t.y., kur kiekviena eilute tures atsirasti, norint, kad funkcija butu tenkinta
                if(maxInd != i)
                    arKeisim = true;
            }
            else {
                return arKonverguoja2(arr);
            }
        }
        if(arKeisim)
            sukeisk(arr, table);
        return true;
    }

    private boolean arKonverguoja2(BigRational[][] arr){    //tikrinu ar konverguoja pagal stulpelius, o ne eilutes.
        int n = arr.length;
        int[] table = new int[n];

        boolean arKeisim = false;
        for(int i = 0; i < n; i++){
            int maxInd = 0;
            BigRational sum = new BigRational(BigInteger.valueOf(0), BigInteger.valueOf(1));
            BigRational max = arr[0][i].abs();  //pasiimu pirmos eilutes atitinkama stulpelis.
            for(int j = 1; j < n; j++){
                BigRational sk = arr[j][i].abs();
                if(sk.compareTo(max) > 0){
                    sum = sum.add(max);
                    maxInd = j;
                    max = sk;
                } else {
                    sum = sum.add(sk);
                }
            }
            if(table[maxInd] == 0 && max.compareTo(sum) > 0) {
                table[maxInd] = i + 1;   //surasau indeksus arr[][], t.y., kur kiekviena eilute tures atsirasti, norint, kad funkcija butu tenkinta
                if(maxInd != i)
                    arKeisim = true;
            }
            else {
                //reiskia sis jau uzimtas ir nekonverguos matrica. Bandoma tuomet su trecia taisykle tikrinti
                return arKonverguoja3(arr);
            }
        }
        if(arKeisim)
            sukeisk(arr, table);
        return true;
    }

    private boolean arKonverguoja3(BigRational[][] arr){
        //jei yra bent vienoje vietoje istrizainis elementas <= kiti tos eilutes el's. Tuomet toje vietoje alfa(ij) = >=1, ir sqrt(all) >= 1, kas reiskia, jog netiks.
        //tad vienintelis budas yra visuomet kelti istrizaini elementa tik didziausia ir tuomet skaiciuoti.
        //tik kad siuo atveju suma visu nebutinai turi buti < istrizainio elemento.

        int n = arr.length;
        int[] table = new int[n];
        boolean arKeisim = false;
        for(int i = 0; i < n; i++){
            int maxInd = 0;
            BigRational max = arr[i][0].abs();
            for(int j = 1; j < n; j++){
                BigRational sk = arr[i][j].abs();
                if(sk.compareTo(max) > 0){  //jei elementas didesnis, tai jis turi eiti i istrizaine
                    maxInd = j;
                    max = sk;
                }
            }
            if(table[maxInd] == 0 && max.compareTo(new BigRational(BigInteger.valueOf(0), BigInteger.valueOf(1))) > 0){
                //tinka tik tokiu atveju, jei istrizaine dar neuzimta IR istrizainis elementas yra nelygus nuliui.
                table[maxInd] = i+1;
                if(maxInd != i){
                    arKeisim = true;
                }
            } else {
                return false;
            }
        }
        if(arKeisim){
            sukeisk(arr, table);
        }

        BigRational powerSums = new BigRational(BigInteger.valueOf(0), BigInteger.valueOf(1));
        BigRational sk;
        for(int i = 0; i < n; i++){
            sk = arr[i][i];
            for(int j = 0; j < i; j++){
                BigRational tmp = (arr[i][j].divide(sk)).abs();   //padalinu is istrizaininio elemento ir sudarau abs value.
                tmp = tmp.multiply(tmp);    //cia tas pats, kas pakelta kvadratu
                powerSums = powerSums.add(tmp);         //pridedu prie tu sumu.
            }
            for(int j = i+1; j < n; j++){
                BigRational tmp = (arr[i][j].divide(sk)).abs();   //padalinu is istrizaininio elemento ir sudarau abs value.
                tmp = tmp.multiply(tmp);    //cia tas pats, kas pakelta kvadratu
                powerSums = powerSums.add(tmp);         //pridedu prie tu sumu.
            }
        }
        if(Math.sqrt(powerSums.doubleValue()) < 1){
            return true;
        }
        return false;
    }

    private void sukeisk(BigRational[][] arr, int[] table){
        int n = table.length;
        BigRational[][] tmpArr = new BigRational[n][n+1];
        for(int i = 0; i < n; i++){
            int index = table[i] - 1;
            tmpArr[i] = arr[index];
        }
        for(int i = 0; i < n; i++){
            arr[i] = tmpArr[i];
        }
    }
}