import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Created by Marius Lk on 2016-09-12.
 */
public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Layouts layoutai = new Layouts(primaryStage);
        primaryStage.setWidth(850);
        primaryStage.setHeight(730);
        layoutai.loadScreen2();
    }
}